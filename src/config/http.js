import { setLoading, auth } from 'src/util/helpers';
import axios from 'axios';
import { usersResource, departmentsResource, coursesResource } from 'src/util/resources';

axios.defaults.headers.common['Authorization'] = auth.getToken();
[usersResource, departmentsResource, coursesResource].forEach(r => {
	// Request interceptor
	r.interceptors.request.use((config) => {
		setLoading(true);
		return config;
	}, (error) => {
		setLoading(false);
		console.log('RequestError: ', error);
		// Do something with request error
		return Promise.reject(error);
	});

	// Response interceptor
	r.interceptors.response.use((response) => {
		setLoading(false);
		if(!response.data.code) {
			return Promise.resolve(response.data.result);
		} else {
			return Promise.reject(response.data.message);
		}
		// return response;
	}, (error) => {
		setLoading(false);
		console.log('ResponseError: ', error);
		// Do something with response error
		return Promise.reject(error);
	});
})

