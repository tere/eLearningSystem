import Home from 'components/Home/home';
import NotFound from 'components/NotFound/notFound';
import Login from 'components/Login/login';
import Departments from 'components/Departments/departments';
import courses from './components/Cursuri/courses';
const routes = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/department',
    name: 'department',
    component: Departments
  },
  {
    path: '/course',
    name: 'course',
    component: courses
  },
  {
    path: '*',
    component: NotFound
  }
];

export default routes;
