import Vue from 'vue';
import template from './departments.html';
import { auth } from 'src/util/helpers';
import './departments.scss';
import { departmentsResource } from 'src/util/resources';
export default Vue.extend({
	template,
	data() {
		return {
			user: {
				email: '',
				password: ''
			},
			departments: [],
			departmentsFilter: '',
			auth: auth,
			selectedDepartment: null,
		}
	},
	created() {
		console.log("In departments")
		this.getAll();
	},
	computed: {
		filteredDepartments() {
			return this.departments.filter((department) => department.name.toLowerCase().indexOf(this.departmentsFilter.toLowerCase()) !== -1);
		}
	},
	methods: {
		getAll() {
			return departmentsResource.get('/').then(response => {
				this.departments = response;
			}).catch(reason => {

			})
		},
		openEditModal(department) {
			this.selectedDepartment = department;
			this.$refs.departmentEditModal.show();
		},
		closeEditModal() {
			this.selectedDepartment = null;
			this.$refs.departmentEditModal.hide();
		},
		newDepartment() {
			this.selectedDepartment = {
				name: "",
				shortName: ""
			}
			this.$refs.departmentEditModal.show();
		},
		saveDepartment() {
			return departmentsResource.post('/', { department: this.selectedDepartment }).then(response => {
				console.log(response);
				this.notify('success', `Department saved`);
				this.getAll();
				this.closeEditModal();
			}).catch(reason => {
				this.notify('warning', `Department not saved`);
				console.log(reason);
			})
		},
		notify(type, message) {
			this.$notify({
				group: 'els',
				title: 'Departments',
				type: type,
				text: message
			});
		}
	}
});
