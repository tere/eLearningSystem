import Vue from 'vue';
import template from './navigation.html';
import { auth } from 'src/util/helpers';
import './navigation.scss';

export default Vue.extend({
	template,
	data() {
		return {
			auth,
		}
	},
	created() {
		console.log(auth.isLoggedIn())
	},
	methods: {
		logout() {
			auth.logout();
			this.$router.push('login');
		}
	}
});
