import Vue from 'vue';
import template from './login.html';
import './login.scss';
import { auth } from 'src/util/helpers';
import { usersResource } from 'src/util/resources';
export default Vue.extend({
	template,
	data() {
		return {
			user: {
				email: '',
				password: ''
			}
		}
	},
	created() {
		console.log("Is loggedIn: ", auth.isLoggedIn())
	},
	methods: {
		login() {
			if (this.user.email && this.user.password) {
				usersResource.post("/login", { email: this.user.email, password: this.user.password }).then(response => {
					auth.login(response.userData, response.token, response.iat);
					this.notify('success', `Hello ${response.userData.name}`);
					setTimeout(() => {
						this.$router.push('department');

						window.location.reload();
					}, 3000);
				}).catch(reason => {
					this.notify('danger', reason);
				});
			}
		},
		notify(type, message) {
			this.$notify({
				group: 'els',
				title: 'User',
				type: type,
				text: message
			});
		}
	}
});
