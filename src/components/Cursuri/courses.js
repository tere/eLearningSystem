import Vue from 'vue';
import template from './courses.html';
import { auth } from 'src/util/helpers';
// import './departments.scss';
import { coursesResource, usersResource } from 'src/util/resources';
export default Vue.extend({
	template,
	data() {
		return {
			courses: [],
			coursesFilter: '',
			auth: auth,
			selectedCourse: null,
		}
	},
	created() {
		console.log("In courses")
		this.getAll();
	},
	computed: {
		filteredCourses() {
			return this.courses.filter((course) => course.name.toLowerCase().indexOf(this.coursesFilter.toLowerCase()) !== -1);
		}
	},
	methods: {
		getAll() {
			return coursesResource.get('/').then(response => {
				console.log(response)
				this.courses = response;
			}).catch(reason => {

			})
		},
		openEditModal(course) {
			this.selectedCourse = course;
			this.$refs.courseEditModal.show();
		},
		closeEditModal() {
			this.selectedCourse = null;
			this.$refs.courseEditModal.hide();
		},
		newCourse() {
			this.selectedCourse = {
				name: "",
				code: ""
			}
			this.$refs.courseEditModal.show();
		},
		saveCourse() {
			return coursesResource.post('/', { course: this.selectedCourse }).then(response => {
				console.log(response);
				this.notify('success', `Course saved`);
				this.getAll();
				this.closeEditModal();
			}).catch(reason => {
				this.notify('warning', `Course not saved`);
				console.log(reason);
			})
		},
		notify(type, message) {
			this.$notify({
				group: 'els',
				title: 'Courses',
				type: type,
				text: message
			});
		},
		signUp(course) {
			let user = auth.user;
			user.courses.push(course.code);
			return usersResource.post('/singupcourse', { user: user }).then(response => {
				auth.updateLocalData(user);
				this.notify('success', `Congratulations for signing up for ${course.name}`);
			}).catch(reason => {
				this.notify('warning', `Can't signup for  ${course.name}. God intervention`);
				console.log(reason);
			})
		}
	}
});
