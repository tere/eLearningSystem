import axios from 'axios';

import { API_BASE } from 'src/config/constants';

// Resources for /posts endpoint on API
// @see https://github.com/mzabriskie/axios#creating-an-instance
export const departmentsResource = axios.create({
  baseURL: `${API_BASE}/department/`
});
export const usersResource = axios.create({
  baseURL: `${API_BASE}/user/`
});
export const coursesResource = axios.create({
  baseURL: `${API_BASE}/course/`
});
