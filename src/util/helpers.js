import { LoadingState } from 'src/config/loading-state';

// Helper to set global loading state
export const setLoading = (isLoading = false) => LoadingState.$emit('toggle', isLoading);

class Authorization {
	constructor() {
		this.user = localStorage.getItem('user') || null;
		this.iat = localStorage.getItem('iat') || null;
		if(!this.iat || this.iat + 5000 < Math.floor(Date.now() / 1000)) {
			this.logout();
		}
		if (this.user) {
			this.user = JSON.parse(this.user);
		}
		console.log("User data: ", this.user, this.iat);
	}

	isLoggedIn() {
		return this.user !== null && this.iat && this.iat + 5000> Math.floor(Date.now() / 1000);
	}

	login(user, token, iat) {
		this.user = user;
		localStorage.setItem('user', JSON.stringify(this.user));
		localStorage.setItem('token', token);
		localStorage.setItem('iat', iat);
	}

	logout() {
		this.user = null;
		localStorage.removeItem('user');
		localStorage.removeItem('token');
		localStorage.removeItem('iat');
	}

	getToken() {
		return localStorage.getItem('token');
	}

	isAdmin() {
		console.log("Is admin: ", this.user && this.user.role.includes('ADMIN'))
		return this.user && this.user.role.includes('ADMIN');
	}

	isStudent() {
		return this.user && this.user.role.includes('STUDENT');
	}

	isSignedUpFor(code) {
		return this.user && this.user.courses.includes(code);
	}

	updateLocalData(user) {
		localStorage.setItem(user, JSON.stringify(user));
	}
}

export const auth = new Authorization();
