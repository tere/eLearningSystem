//Other Topic Model
'use strict';
const mongoose = require('mongoose');
const CourseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	code: {
		type: Number,
		required: true
	},
}, {
    toObject: {
        virtuals: true
    },
    toJson: {
        virtuals: true
    }
});

CourseSchema.index({
	name: 1,
	code: 1
});


module.exports = mongoose.model('Course', CourseSchema, 'Course');
