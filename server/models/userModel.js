//Other Topic Model
'use strict';
const mongoose = require('mongoose');
const UserSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	role: {
		type: [String],
		enum: ['ADMIN', 'PROFESSOR', 'STUDENT']
	},
	courses: {
		type: [Number],
		required: false
	},
	password: {
		type: String,
		required: true
	}
}, {
    toObject: {
        virtuals: true
    },
    toJson: {
        virtuals: true
    }
});

UserSchema.index({
	email: 1
});


module.exports = mongoose.model('User', UserSchema, 'User');
