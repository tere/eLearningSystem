//Other Topic Model
'use strict';
const mongoose = require('mongoose');
const DepartmentSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	shortName: {
		type: String,
		required: true
	},
}, {
    toObject: {
        virtuals: true
    },
    toJson: {
        virtuals: true
    }
});

DepartmentSchema.index({
	name: 1,
	shortName: 1
});


module.exports = mongoose.model('Department', DepartmentSchema, 'Department');
