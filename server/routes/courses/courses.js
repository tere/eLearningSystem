const CourseModel = require('../../models/courseModel');


let getCourses = function (id) {
	return new Promise((resolve, reject) => {
		let conditions = (id) ? { _id: id } : {};
		CourseModel.find(conditions).exec().then(courses => {
			if(id) {
				return resolve(courses.length ? courses[0] : null);
			}
			return resolve(courses);
		}).catch(reason => {
			return reject({code: 501, message: "Can't get"});
		});
	})
}


let createOrUpdateCourse = function(course) {
	return new Promise((resolve, reject) => {
		if(course.hasOwnProperty('_id')) {
			CourseModel.findOneAndUpdate({
				_id: course._id
			}, course, {new: true}, (err, doc) => {
				if(err) {
					return reject({code: 502, message: "Can't update"});
				}
				return resolve(doc);
			});
		} else {
			CourseModel.create(course, (err, res) => {
				if(err) {
					console.log(err);
					return reject({code: 503, message: "Can't insert"});
				}
				return resolve(res);
			});
		}
	})
}


let deleteCourse = function(courseId) {
	return new Promise((resolve, reject) => {
		CourseModel.find({ _id: courseId }).remove((err) => {
			if(err) {
				return reject({code: 504, message: "Can't delete"});
			}
			return resolve(null);
		});
	});
}


module.exports = {
	getCourses,
	createOrUpdateCourse,
	deleteCourse
}
