var express = require('express');
var router = express.Router();
let doResponse = require('../../utils/common').doResponse;
let courses = require('./courses');


router.get('/', (req, res, next) => {
	courses.getCourses().then(cs => {
		return res.send(doResponse(cs));
	}).catch(reason => {
		return res.send(doResponse(reason.code, reason.message));
	});
});

router.get('/:id', (req, res) => {
	courses.getCourses(req.params.id).then(c => {
		return res.send(doResponse(c));
	}).catch(reason => {
		return res.send(doResponse(reason.code, reason.message));
	});
});

router.post('/', (req, res) => {
	courses.createOrUpdateCourse(req.body.course).then(c => {
		return res.send(doResponse(c));
	}).catch(reason => {
		return res.send(doResponse(reason.code, reason.message));
	});
});

router.delete('/:id', (req, res) => {
	courses.deleteCourse(req.params.id).then(result => {
		return res.send(doResponse(result));
	}).catch(reason => {
		return res.send(doResponse(reason.code, reason.message));
	});
})
module.exports = router;
