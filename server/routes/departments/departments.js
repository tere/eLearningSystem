const DepartmentModel = require('../../models/departmentModel');


let getDepartments = function (id) {
	return new Promise((resolve, reject) => {
		let conditions = (id) ? { _id: id } : {};
		DepartmentModel.find(conditions).exec().then(departments => {
			if(id) {
				return resolve(departments.length ? departments[0] : null);
			}
			return resolve(departments);
		}).catch(reason => {
			return reject({code: 501, message: "Can't get"});
		});
	})
}


let createOrUpdateDepartment = function(department) {
	return new Promise((resolve, reject) => {
		if(department.hasOwnProperty('_id')) {
			DepartmentModel.findOneAndUpdate({
				_id: department._id
			}, department, {new: true}, (err, doc) => {
				if(err) {
					return reject({code: 502, message: "Can't update"});
				}
				return resolve(doc);
			});
		} else {
			DepartmentModel.create(department, (err, res) => {
				if(err) {
					return reject({code: 503, message: "Can't insert"});
				}
				return resolve(res);
			});
		}
	})
}


let deleteDepartment = function(departmentId) {
	return new Promise((resolve, reject) => {
		DepartmentModel.find({ _id: departmentId }).remove((err) => {
			if(err) {
				return reject({code: 504, message: "Can't delete"});
			}
			return resolve(null);
		});
	});
}


module.exports = {
	getDepartments,
	createOrUpdateDepartment,
	deleteDepartment
}
