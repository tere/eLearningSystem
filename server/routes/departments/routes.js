var express = require('express');
var router = express.Router();
let doResponse = require('../../utils/common').doResponse;
let departments = require('./departments');


router.get('/', (req, res, next) => {
	departments.getDepartments().then(deps => {
		return res.send(doResponse(deps));
	}).catch(reason => {
		return res.send(doResponse(reason.code, reason.message));
	});
});

router.get('/:id', (req, res) => {
	departments.getDepartments(req.params.id).then(deps => {
		return res.send(doResponse(deps));
	}).catch(reason => {
		return res.send(doResponse(reason.code, reason.message));
	});
});

router.post('/', (req, res) => {
	departments.createOrUpdateDepartment(req.body.department).then(dep => {
		return res.send(doResponse(dep));
	}).catch(reason => {
		return res.send(doResponse(reason.code, reason.message));
	});
});

router.delete('/:id', (req, res) => {
	departments.deleteDepartment(req.params.id).then(result => {
		return res.send(doResponse(result));
	}).catch(reason => {
		return res.send(doResponse(reason.code, reason.message));
	});
})
module.exports = router;
