const UserModel = require('../../models/userModel');
const jwt = require('jsonwebtoken');
const passkey = 'marianmv';
const md5 = require('md5');

let getUsers = function (id) {
	return new Promise((resolve, reject) => {
		let conditions = (id) ? { _id: id } : {};
		UserModel.find(conditions).exec().then(users => {
			if (id) {
				return resolve(users.length > 0 ? users[0] : null);
			}
			return resolve(users);
		}).catch(reason => {
			return reject({ code: 501, message: "Can't get" });
		});
	})
}


let createOrUpdateUser = function (user) {
	return new Promise((resolve, reject) => {
		if (user.hasOwnProperty('_id')) {
			UserModel.findOneAndUpdate({
				_id: user._id
			}, user, { new: true }, (err, doc) => {
				if (err) {
					return reject({ code: 502, message: "Can't update" });
				}
				return resolve(doc);
			});
		} else {
			UserModel.create(user, (err, res) => {
				if (err) {
					return reject({ code: 503, message: "Can't insert" });
				}
				return resolve(res);
			});
		}
	})
}


let deleteUser = function (userId) {
	return new Promise((resolve, reject) => {
		UserModel.find({ _id: userId }).remove((err) => {
			if (err) {
				return reject({ code: 504, message: "Can't delete" });
			}
			return resolve(null);
		});
	});
}


let login = function (email, password) {
	return new Promise((resolve, reject) => {
		UserModel.findOne({
			email: email
		}).exec().then(user => {
			if (!user) {
				return reject({ code: 403, message: "Incorrect credentials" });
			}
			if (!md5(password) === user.password) {
				return reject({ code: 403, message: "Incorrect credentials" });
			}
			delete user.password;
			jwt.sign({ user }, passkey, (err, token) => {
				console.log(token);
				return resolve({
					userData: user,
					token: token,
					iat: Math.floor(Date.now() / 1000)
				});
			});
		})
	})
}

let authorize = function () {
	var token = req.body.token || req.query.token || req.headers['authorization'] || req.headers['x-access-token'];
	// decode token
	if (token) {
		// verifies secret and checks exp
		jwt.verify(token, passkey, function (err, decoded) {
			if (err) {
				console.log(err);
				return res.send({ code: 401, message: 'Failed to authenticate token.' })
			} else {
				// if everything is good, save to request for use in other routes
				req.decoded = decoded;
				console.log(decoded);
				// if(scopes) {
				// 	console.log('i get here', scopes);
				// } else {
				// }
				next();
			}
		});
	} else {
		// if there is no token
		// return an error
		return res.send({ code: 403, message: "Must be logged in for this" })
	}
}

let signForCourse = (user) => {
	return new Promise((resolve, reject) => {
		delete user._id;
		UserModel.findOneAndUpdate(
			{
				email: user.email
			}, user,
			(err, doc, res) => {
				if (err) {
					console.log(err);
					return reject({ code: 504, message: "Error" })
				}
				return resolve(doc)
			}
		)
	});
}

module.exports = {
	getUsers,
	createOrUpdateUser,
	deleteUser,
	login,
	signForCourse
}
