var express = require('express');
var router = express.Router();
let doResponse = require('../../utils/common').doResponse;
let users = require('./users');


router.post('/login', (req, res, next) => {
	users.login(req.body.email, req.body.password).then(login => {
		return res.send(doResponse(login));
	}).catch(reason => {
		console.log(reason);
		return res.send(doResponse(reason.code, reason.message));
	});
});


router.post('/singupcourse', (req, res) => {
	users.signForCourse(req.body.user).then(response => {
		return res.send(doResponse(response));
	}).catch(reason => {
		console.log(reason);
		return res.send(doResponse(reason.code, reason.message));
	});
})

module.exports = router;
