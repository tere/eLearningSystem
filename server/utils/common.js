const doResponse = (...args) => {
    let response = {
		code: null,
		message: null,
		result: null
    }
    if(args.length === 1) {
        response.result = args[0];
    } else {
        response.code = args[0];
        response.message = args[1];
    }
    return response;
}



module.exports = {
	doResponse
}
