var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const mongoose = require('mongoose');
const fs = require('fs');
const cors = require('cors');

const mongoAppDbName = "eLearningSystem";

const mongo = {
	creds: 'andreiterecoasa:Tere-1993',
	hosts: [
		'cluster0-shard-00-00-atttm.mongodb.net:27017',
		'cluster0-shard-00-01-atttm.mongodb.net:27017',
		'cluster0-shard-00-02-atttm.mongodb.net:27017'
	]
}

//andreiterecoasa - Tere-1993
let mongoURI = `mongodb://${mongo.creds}@${mongo.hosts[0]},${mongo.hosts[1]},${mongo.hosts[2]}/${mongoAppDbName}?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin`;

mongoose.Promise = global.Promise;
const connectionPromise = mongoose.connection.openUri(mongoURI);

connectionPromise.then((db) => {
	console.log(` ==== Connected to database ${mongoAppDbName} ==== `);
}).catch((err) => {
	console.error(`Rejected promise: ${err}`);
	mongoose.disconnect();
});

require('./seed');

//init mongo models;
let models = './models';
fs.readdir(models, (err, files) => {
	files.forEach(file => {
		require(models + "/" + file);
	})
})

var app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors('*'));


app.use('/department', require('./routes/departments/routes'));
app.use('/user', require('./routes/users/routes'));
app.use('/course', require('./routes/courses/routes'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};
	res.send(err);
});

module.exports = app;
